# Implementing-insert-method
Implementing insert method
@Override
public Uri insert(Uri uri, ContentValues values) {
      // Validate the requested uri
  if (sUriMatcher.match(uri) != INCOMING_BOOK_COLLECTION_URI_INDICATOR) {
       throw new IllegalArgumentException("Unknown URI " + uri);
    }
    
    //validate input fields
    //Make sure that the fields are all set
    if (values.containKey(BookTableMetaData.CREATED_DATE) == false) {
        values.put(BookTableMetaData.MODIFIED_DATE, now);
    }
    
    if (values.containsKey(BookTableMetaData.BOOK_NAME) == false) {
        throw new SQLException(
            "Failed to insert row because BookName is needed " + uri);
    }
    
    if(values.containsKey(BookTableMetaData.BOOK_ISBN == false) {
       values.put(BookTableMetaData.BOOK_ISBN, "Unknown Author")
    }
    
    SQliteDataBase db = mOpenHelper.getWritableDatabase();
    long rowId = db.insert(BookTableMetaData.TABLE_NAME
                                     , BookTableMetaData.BOOK_NAME, value);
    if (rowId > 0) {
        Uri insertedBookUri = ContentUris.withAppendedId(
                               BookTableMetaData.CONTENT_URI, rowId);
        getContext().getContentResolver().notifyChange(insertedBookUri, null);
        return insertedBookUri;
      }
      
      throw new SQLException("Failed to insert row into " + uri);
    }
    
